import 'package:tenizo/layout/help_page.dart';
import 'package:tenizo/layout/login.dart';
import 'package:flutter/material.dart';
import 'package:tenizo/layout/privacy_policy.dart';
import 'package:tenizo/layout/splash_screen.dart';
import 'package:tenizo/layout/MainPage.dart';
import 'layout/help.dart';
import 'layout/terms_conditions.dart';
import 'util/app_const.dart';

setRoute(settings) {
  switch (settings.name) {
    case '/':
      {
        return MaterialPageRoute<bool>(
          builder: (BuildContext context) => SplashScreen(),
        );
      }
    case '/MainPage':
      {
        return MaterialPageRoute<bool>(
          builder: (BuildContext context) =>MainPage(selectedPage: RoutingData.Home),
        );
      }
    case '/Login':
      {
        return MaterialPageRoute<bool>(
          builder: (BuildContext context) => Login(),
        );
      }
       case '/PrivacyPolicy':
      {
        return MaterialPageRoute<bool>(
          builder: (BuildContext context) => PrivacyPolicy(),
        );
      }
       case '/TermsConditionsPage':
      {
        return MaterialPageRoute<bool>(
          builder: (BuildContext context) => TermsConditionsPage(),
        );
      }
       case '/Help':
      {
        return MaterialPageRoute<bool>(
          builder: (BuildContext context) => Help(),
        );
      }
    default:
      {
        return null;
      }
  }
}
