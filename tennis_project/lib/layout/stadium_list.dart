import 'dart:io';

import 'package:tenizo/controller/stadium_data.dart';
import 'package:tenizo/custom/custom_expansion_tile.dart';
import 'package:tenizo/model/color_loader.dart';
import 'package:tenizo/styles/app_style.dart';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:tenizo/util/app_const.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tenizo/util/popup_without_pop.dart';

class StadiumList extends StatefulWidget {
  final Function onSelected;
  final Function onArgumentSaving;
  final String userName;

  @override
  _StadiumListState createState() => _StadiumListState();

  StadiumList({Key key, this.onSelected, this.onArgumentSaving, this.userName})
      : super(key: key);
}

class _StadiumListState extends State<StadiumList> with BaseControllerListner {
  BaseController controller;

  @override
  void initState() {
    super.initState();

    controller = new BaseController(this);
    _loadData();
  }

  _loadData() {
    var param2 = [
      "SELECT * FROM stadium  where status = ? and user_name = ?",
      [0, widget.userName],
      {"calledMethod": 'selectVal_1'}
    ];

    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param2);
    var param1 = [
      "SELECT * FROM court  where status = ?",
      [0],
      {"calledMethod": 'selectVal_2'}
    ];

    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param1);
  }

  List<Widget> _listOfExpansions = [];
  List<Stadium> _listStatdium = [];
  List<Court> _listCourt = [];

  static var datastatus = 0;

  bool _loading = true;

  static var celectedStadiumId;

  deleteStadium(List<String> paramList) {
    var dataId = paramList[0];
    var type = paramList[1];
    var stadId = paramList[3];

    if (type == "stadium") {
      _deleteDirectoryImage(paramList[2]); //delete image from driectory
      var param = [
        "UPDATE stadium SET status = ? where stadium_id = ?",
        [1, dataId],
        {"calledMethod": 'deletStadium'}
      ];
      controller.execFunction(
          ControllerFunc.db_sqlite, ControllerSubFunc.db_update, param);
    } else if (type == "court") {
      _deleteDirectoryImage(paramList[2]); //delete image from driectory
      var param = [
        "UPDATE court SET status = ? where court_id = ?",
        [1, dataId],
        {"calledMethod": 'deletCourt'}
      ];
      controller.execFunction(
          ControllerFunc.db_sqlite, ControllerSubFunc.db_update, param);

      setState(() {
        celectedStadiumId = stadId;
      });
    } else {}
    {}
  }

  setStatdiumData(data) async {
    data.forEach((i) {
      _listStatdium.add(Stadium(
        stadiumName: i['stadium_name'],
        stadiumId: i['stadium_id'],
        stadiumAddress: i['stadium_address'],
        contactNumber: i['contact_number'].toString(),
        noOfCourts: i['no_of_courts'],
        reservationUrl: i['reservation_url'],
        stadiumImage: i['stadium_image'],
        stadiumLat: i['Stadium_Lat'],
        stadiumLon: i['Stadium_Lon'],
      ));
    });
  }

  setCourtData(data) {
    data.forEach((i) {
      _listCourt.add(Court(
        courtName: i['court_name'],
        stadiumName: i['stadium_name'],
        stadiumId: i['stadium_id'],
        courtId: i['court_id'],
        serface: i['serface'],
        indoor: i['indoor'],
        availableTime: i['available_time'],
        courtImage: i['court_image'],
      ));
    });
  }

  //Stadium View/remove/edit popup functions
  _stadiumPopUpActions(choice, s) async {
    if (choice == "View") {
      widget.onArgumentSaving({
        'stadiumName': s.stadiumName,
        'stadiumId': s.stadiumId,
        'stadiumAddress': s.stadiumAddress,
        'contactNumber': s.contactNumber,
        'noOfCourts': s.noOfCourts,
        'reservationUrl': s.reservationUrl,
        'stadiumImage': s.stadiumImage,
        'stadiumLat': s.stadiumLat,
        'stadiumLon': s.stadiumLon
      });
      widget.onSelected(RoutingData.StadiumDetails, false, true);
    } else if (choice == "Edit") {
      widget.onArgumentSaving({
        'stadiumName': s.stadiumName,
        'stadiumId': s.stadiumId,
        'stadiumAddress': s.stadiumAddress,
        'contactNumber': s.contactNumber,
        'noOfCourts': s.noOfCourts,
        'reservationUrl': s.reservationUrl,
        'stadiumImage': s.stadiumImage,
        'stadiumLat': s.stadiumLat,
        'stadiumLon': s.stadiumLon
      });
      widget.onSelected(RoutingData.StadiumEdit, false, true);
    } else if (choice == "Delete") {
      showDialog(
          context: context,
          builder: (context) => CustomAppPopUpForDelete(context, "CSDelete",
              [s.stadiumId, "stadium", s.stadiumImage, ""], deleteStadium));
    }
  }

  //court View/remove/edit popup functions
  _courtPopUpActions(choice, data) async {
    if (choice == "View") {
      widget.onArgumentSaving({
        'courtId': data.courtId,
        'courtName': data.courtName,
        'courtSurface': data.serface,
        'courtIndoor': data.indoor,
        'courtTime': data.availableTime,
        'courtImage': data.courtImage,
      });
      widget.onSelected(RoutingData.CourtView, false, true);
    } else if (choice == "Edit") {
      widget.onArgumentSaving({
        'courtId': data.courtId,
        'courtName': data.courtName,
        'courtSurface': data.serface,
        'courtIndoor': data.indoor,
        'courtTime': data.availableTime,
        'courtImage': data.courtImage,
      });
      widget.onSelected(RoutingData.CourtEdit, false, true);
    } else if (choice == "Delete") {
      showDialog(
          context: context,
          builder: (context) => CustomAppPopUpForDelete(
              context,
              "CSDelete",
              [data.courtId, "court", data.courtImage, data.stadiumId],
              deleteStadium));
    }
  }

  _deleteDirectoryImage(image) async {
    if (image != null) {
      final delFir = File(image);
      if (delFir.existsSync()) {
        delFir.deleteSync(recursive: true); //for delete
      }
    }
  }

  setExpansionList() {
    _listStatdium.forEach((s) {
      List<Court> _courts = _listCourt.where((value) {
        return value.stadiumId == s.stadiumId;
      }).toList();

      setState(() {
        _listOfExpansions
            .add(Stack(alignment: Alignment.topRight, children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 17),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: AppStyle.color_boderBox,
                  width: 1.0,
                ),
              ),
            ),
            child: Padding(
//court popup---------------------
              padding: const EdgeInsets.only(bottom: 8.0),
              child: CustomExpansionTile(
                title: s.stadiumName,
                listData: _courts
                    .map(
                      (data) => Container(
                        color: AppStyle.color_dropBox,
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                trailing: PopupMenuButton(
                                  onSelected: (choice) {
                                    _courtPopUpActions(choice, data);
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 10.0, right: 10),
                                    child: Icon(
                                      Icons.more_vert,
                                      color: AppStyle.color_Head,
                                      size: 28,
                                    ),
                                  ),
                                  itemBuilder: (context) =>
                                      <PopupMenuItem<String>>[
                                    PopupMenuItem<String>(
                                      value: 'View',
                                      child: SizedBox.expand(
                                          child: Row(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: 10.0),
                                            child: Icon(
                                              Icons.visibility,
                                              size: 28,
                                            ),
                                          ),
                                          Text(
                                            'View',
                                            style: TextStyle(fontSize: 20),
                                          ),
                                        ],
                                      )),
                                    ),
                                    PopupMenuItem<String>(
                                      value: 'Edit',
                                      child: SizedBox.expand(
                                          child: Row(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: 10.0),
                                            child: Icon(
                                              Icons.edit,
                                              size: 28,
                                            ),
                                          ),
                                          Text(
                                            'Edit',
                                            style: TextStyle(fontSize: 20),
                                          ),
                                        ],
                                      )),
                                    ),
                                    PopupMenuItem<String>(
                                        value: 'Delete',
                                        child: SizedBox.expand(
                                          child: Row(
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 10.0),
                                                child: Icon(
                                                  Icons.delete,
                                                  size: 28,
                                                ),
                                              ),
                                              Text(
                                                'Delete',
                                                style: TextStyle(fontSize: 20),
                                              ),
                                            ],
                                          ),
                                        )),
                                  ],
                                ),
                                title: Text(data.courtName ,
                                  overflow: TextOverflow.ellipsis),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                    .toList(),
                onButtonPressed: () {
                  widget.onArgumentSaving({
                    'stadiumName': s.stadiumName,
                    'stadiumId': s.stadiumId,
                  });
                  widget.onSelected(RoutingData.CourtRegistration, false, true);
                },
              ),
            ),
          ),
          PopupMenuButton(
            onSelected: (choice) {
              _stadiumPopUpActions(choice, s);
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 15.0, right: 10),
              child: Icon(
                Icons.more_vert,
                color: AppStyle.color_Head,
                size: 28,
              ),
            ),
            itemBuilder: (context) => <PopupMenuItem<String>>[
              PopupMenuItem<String>(
                value: 'View',
                child: SizedBox.expand(
                    child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 10.0),
                      child: Icon(
                        Icons.visibility,
                        size: 28,
                      ),
                    ),
                    Text(
                      'View',
                      style: TextStyle(fontSize: 20),
                    ),
                  ],
                )),
              ),
              PopupMenuItem<String>(
                value: 'Edit',
                child: SizedBox.expand(
                    child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 10.0),
                      child: Icon(
                        Icons.edit,
                        size: 28,
                      ),
                    ),
                    Text(
                      'Edit',
                      style: TextStyle(fontSize: 20),
                    ),
                  ],
                )),
              ),
              PopupMenuItem<String>(
                  value: 'Delete',
                  child: SizedBox.expand(
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: Icon(
                            Icons.delete,
                            size: 28,
                          ),
                        ),
                        Text(
                          'Delete',
                          style: TextStyle(fontSize: 20),
                        ),
                      ],
                    ),
                  )),
            ],
          ),
        ]));
      });
    });

    _listOfExpansions.add(
      Container(
        alignment: Alignment(1.0, 1.0),
        height: 86,
        color: AppColors.white,
      ),
    );
  }

  static var deviceWidth = 0.0;
  static var currentWidth = 0.0;
  static var deviceHeight = 0.0;
  static var currentHeight = 0.0;

  var paddingData;
  var paddingData3;

  var buttonWidth = 60.0;
  var buttonHeight = 60.0;
  var iconSize = 40.0;

  var playerBTNWidth = 70.0;
  var playerBTNHeigh = 70.0;
  var playerBTNicon = 60.0;

  var paddingData2 = EdgeInsets.only(
      right: 30.0, top: 60.0, bottom: 10, left: currentWidth / 12 * 8);

  _setDeviceWidth() {
    if (deviceWidth <= 350) {
      setState(() {
        currentWidth = 300;
      });
      paddingData2 = EdgeInsets.only(
          right: 30.0, top: 60.0, bottom: 10, left: currentWidth / 12 * 8);
    } else if (deviceWidth <= 450) {
      currentWidth = 380;
      paddingData2 = EdgeInsets.only(
          right: 30.0, top: 60.0, bottom: 10, left: currentWidth / 12 * 8);
    } else if (deviceWidth <= 550) {
      currentWidth = 450;
      paddingData2 = EdgeInsets.only(
          right: 30.0, top: 60.0, bottom: 10, left: currentWidth / 12 * 9);
    } else if (deviceWidth <= 750) {
      currentWidth = 450;
      paddingData2 = EdgeInsets.only(
          right: 30.0, top: 60.0, bottom: 10, left: currentWidth / 12 * 9);
    } else if (deviceWidth > 750) {
      currentWidth = 700;
      paddingData2 = EdgeInsets.only(
          right: 30.0, top: 60.0, bottom: 10, left: currentWidth / 12 * 11);
    } else {
      currentWidth = 600;
      paddingData2 = EdgeInsets.only(
          right: 30.0, top: 60.0, bottom: 10, left: currentWidth / 12 * 11);
    }

    if (deviceHeight <= 500) {
      currentHeight = 300;

      paddingData = const EdgeInsets.only(top: 30.0, left: 190);
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 5);

      buttonWidth = 60;
      buttonHeight = 60;
      iconSize = 40;

      playerBTNWidth = 50;
      playerBTNHeigh = 50;
      playerBTNicon = 40;
    } else if ((deviceHeight <= 600)) {
      currentHeight = 350;

      paddingData = const EdgeInsets.only(top: 30.0, left: 170, right: 0.0);
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 5);

      buttonWidth = 60;
      buttonHeight = 60;
      iconSize = 40;

      playerBTNWidth = 60;
      playerBTNHeigh = 60;
      playerBTNicon = 50;
    } else if ((deviceHeight <= 700)) {
      currentHeight = 400;

      paddingData = const EdgeInsets.only(top: 50.0, left: 240, right: 00.0);
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 6);

      buttonWidth = 60;
      buttonHeight = 60;
      iconSize = 40;

      playerBTNWidth = 60;
      playerBTNHeigh = 60;
      playerBTNicon = 50;
    } else if ((deviceHeight <= 800)) {
      currentHeight = 400;
      paddingData = const EdgeInsets.only(top: 50.0, left: 300, right: 05.0);
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 6);

      buttonWidth = 60;
      buttonHeight = 60;
      iconSize = 40;

      playerBTNWidth = 60;
      playerBTNHeigh = 60;
      playerBTNicon = 50;
    } else if ((deviceHeight <= 900)) {
      currentHeight = 450;

      paddingData = const EdgeInsets.only(top: 50.0, left: 350, right: 00.0);
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 5);

      buttonWidth = 70;
      buttonHeight = 70;
      iconSize = 50;
    } else if (deviceHeight <= 1000) {
      currentHeight = 500;

      paddingData = const EdgeInsets.only(top: 50.0, left: 325, right: 0.0);
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 5);

      buttonWidth = 70;
      buttonHeight = 70;
      iconSize = 50;
    } else if (deviceHeight > 1000) {
      currentHeight = 600;

      paddingData = const EdgeInsets.only(top: 100.0, left: 640, right: 0.0);
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 5);

      buttonWidth = 70;
      buttonHeight = 70;
      iconSize = 50;
    } else {
      currentHeight = 450;
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 5);
      paddingData = const EdgeInsets.only(top: 50.0, left: 340, right: 0.0);
    }
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      deviceWidth = MediaQuery.of(context).size.width;
      deviceHeight = MediaQuery.of(context).size.height;
    });
    _setDeviceWidth();

    return WillPopScope(
      child: MediaQuery(
        data: MediaQueryData(),
        child: _loading
            ? Container(
                color: Colors.white, child: Center(child: ColorLoader()))
            : LayoutBuilder(
                builder: (ctx, c) {
                  if (datastatus == 0) {
                    return Scaffold(
                      backgroundColor: Color(0xFFE5F9E0),
                      body: SizedBox.expand(
                        child: Container(
                          alignment: Alignment.center,
                          child: SingleChildScrollView(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, right: 5),
                                  child: Padding(
                                    padding: paddingData3,
                                    child: Container(
                                      width: currentWidth,
                                      height: currentHeight - 180,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        border: Border.all(
                                          color: Colors.white,
                                          style: BorderStyle.solid,
                                        ),
                                      ),
                                      child: Center(
                                        child: Text(
                                          "Add Your First Stadium",
                                          style: TextStyle(
                                              fontSize: 30,
                                              color: AppStyle.color_Head,
                                              fontWeight: FontWeight.w600,
                                              fontFamily: 'Rajdhani'),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Padding(
                                      padding: paddingData2,
                                      
                                      child: Container(
                                        // color: Colors.yellow,
                                        width: 50,
                                        height: 60,
                                        decoration: BoxDecoration(
                                          image: new DecorationImage(
                                            fit: BoxFit.fill,
                                            image: new AssetImage(
                                                "images/down_arrow.png"),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: paddingData2,
                                      
                                      child: Container(
                                        width: buttonWidth,
                                        height: buttonHeight,
                                        decoration: BoxDecoration(
                                          color: AppStyle.color_Head,
                                          borderRadius:
                                              BorderRadius.circular(50.0),
                                          border: Border.all(
                                            color: AppStyle.color_Head,
                                            style: BorderStyle.solid,
                                          ),
                                        ),
                                        child: FloatingActionButton(
                                          onPressed: () {
                                            widget.onSelected(
                                                RoutingData.StadiumRegistration,
                                                false,
                                                true);
                                          },
                                          isExtended: true,
                                          materialTapTargetSize:
                                              MaterialTapTargetSize.shrinkWrap,
                                          backgroundColor: AppStyle.color_Head,
                                          child: Icon(
                                            Icons.add,
                                            size: iconSize,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  } else {
                    return Scaffold(
                      backgroundColor: AppColors.white,
                      floatingActionButton: Container(
                        width: playerBTNWidth,
                        height: playerBTNHeigh,
                        child: FloatingActionButton(
                          onPressed: () {
                            widget.onSelected(
                                RoutingData.StadiumRegistration, false, true);
                          },
                          isExtended: true,
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          backgroundColor: AppStyle.color_Head,
                          child: Icon(
                            Icons.add,
                            size: playerBTNicon,
                          ),
                        ),
                      ),
                      body: Container(
                        child: ListView(
                          padding: EdgeInsets.all(8.0),
                          children: _listOfExpansions
                              .map((expansionTile) => expansionTile)
                              .toList(),
                        ),
                      ),
                    );
                  }
                },
              ),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

  @override
  resultFunction(func, subFunc, response) async {
    if (subFunc == ControllerSubFunc.db_select) {
      if (response["calledMethod"] == "selectVal_1") {
        setStatdiumData(response['response_data']);

        setState(() {
          datastatus = response['response_data'].length;
          _loading = false;
        });
      } else if (response["calledMethod"] == "selectVal_2") {
        await setCourtData(response['response_data']);
        setExpansionList();
      }
      if (response["calledMethod"] == "selectNoFoCourts") {
        var courtNo = (response['response_data'][0]['no_of_courts']) - 1;

        var param1 = [
          "UPDATE stadium SET no_of_courts = ? where stadium_id = ?",
          [courtNo, celectedStadiumId],
          {"calledMethod": 'updateNoFoCourts'}
        ];
        controller.execFunction(
            ControllerFunc.db_sqlite, ControllerSubFunc.db_update, param1);
      } else {}
    } else if (subFunc == ControllerSubFunc.db_update) {
      if (response["calledMethod"] == "deletStadium") {
        Navigator.of(context).pop();
        setState(() {
          _listCourt = [];
          _listStatdium = [];
          _listOfExpansions = [];
        });
        _loadData();
      }

      if (response["calledMethod"] == "deletCourt") {
        var param2 = [
          "SELECT no_of_courts FROM stadium  where stadium_id = ?",
          [celectedStadiumId],
          {"calledMethod": 'selectNoFoCourts'}
        ];

        controller.execFunction(
            ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param2);
      } else if (response["calledMethod"] == "updateNoFoCourts") {
        Navigator.of(context).pop();
        setState(() {
          _listCourt = [];
          _listStatdium = [];
          _listOfExpansions = [];
        });
        _loadData();
      } else {}
    } else {}
  }
}
