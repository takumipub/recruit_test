import 'package:flutter/material.dart';
import 'package:tenizo/packages/fancy_nav/fancy_bottom_navigation.dart';

class BottomBar extends StatefulWidget implements PreferredSizeWidget {
  final String headerTitle;
  final int selectedPage;
  final Function setPage;
  final GlobalKey bottomNavigationKey;

  BottomBar(
      {Key key,
      this.headerTitle,
      this.selectedPage,
      this.bottomNavigationKey,
      @required this.setPage})
      : super(key: key);

  @override
  Size get preferredSize => new Size.fromHeight(kToolbarHeight);
  @override
  State<StatefulWidget> createState() {
    return _BottomBarX();
  }
}

class _BottomBarX extends State<BottomBar> {
  int selectedPage = 3;
  @override
  void initState() {
    super.initState();
    this.selectedPage = widget.selectedPage;
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQueryData(),
      child: FancyBottomNavigation(
        initialSelection: selectedPage,
        textColor: Color(0xFFFFFFFF),
        barBackgroundColor: Color(0xFF00D9D9),
        circleColor: Color(0xFFFFFFFF),
        activeIconColor: Color(0xFF000000),
        inactiveIconColor: Color(0xFFFFFFFF),
        key: widget.bottomNavigationKey,
        tabs: [
          TabData(iconData: Icons.group_add, title: "Players"),
          TabData(iconData: Icons.pages, title: "Stadium"),
          TabData(iconData: Icons.add, title: "Match"),
          TabData(iconData: Icons.home, title: "Home")
        ],
        onTabChangedListener: (position) {
          widget.setPage(position);
        },
      ),
    );
  }
}
