class EditPlayerDetailsUtils {
  static ageCalculation(var today, var dateObject) {
    final dob = (dateObject);
    Duration dur = today.difference(dob);
    String differenceInYears = (dur.inDays / 365).floor().toString();

    return differenceInYears;
  }

    static dateFormatter(var date) {
    var selectedyear = date.year;
    var selectedmonth = 'Jan';
    var selectedDate = date.day;
    switch (date.month) {
      case 1:
        selectedmonth = 'Jan';
        break;
      case 2:
        selectedmonth = 'Feb';
        break;
      case 3:
        selectedmonth = 'Mar';
        break;
      case 4:
        selectedmonth = 'Apr';
        break;
      case 5:
        selectedmonth = 'May';
        break;
      case 6:
        selectedmonth = 'Jun';
        break;
      case 7:
        selectedmonth = 'Jul';
        break;
      case 8:
        selectedmonth = 'Aug';
        break;
      case 9:
        selectedmonth = 'Sept';
        break;
      case 10:
        selectedmonth = 'Oct';
        break;
      case 11:
        selectedmonth = 'Nov';
        break;
      case 12:
        selectedmonth = 'Dec';
        break;
      default:
    }
    return '$selectedmonth $selectedDate , $selectedyear';
  }
}