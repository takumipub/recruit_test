import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:tenizo/const.dart';
import 'package:tenizo/tennizo_controller_functions.dart';

class SQLiteController {
  Database db;
  var result;
  SQLiteControllerListener _listener;
  SQLiteController(this._listener);

  //Clear Result
  void clearResult() {
    result = null;
  }

  //Return result
  getResult() {
    return result;
  }

  //Intialize database
  Future<bool> init_db() async {
    try {
      var databasesPath = await getDatabasesPath();
      String path = join(databasesPath, Const.dbTennizo);
      db = await openDatabase(path, version: 1);

      //Create tables
      await db.transaction((txn) async {
        Const.dbTennizoTables.forEach((tbl) async {
          await txn.execute(tbl);
        });
      });
      return true;
    } on Exception {
      return false;
    }
  }

  //Excecute queries
  Future<bool> execQuery(subFunc, query, params) async {
    try {
      if (subFunc == null) {
        return false;
      }
      switch (subFunc) {
        case ControllerSubFunc.db_select:
          {
            await execSelect(subFunc, query, params);
            break;
          }
        case ControllerSubFunc.db_insert:
          {
            await execInsert(subFunc, query, params);
            break;
          }
        case ControllerSubFunc.db_update:
          {
            await execUpdate(subFunc, query, params);
            break;
          }
        case ControllerSubFunc.db_delete:
          {
            await execDelete(subFunc, query, params);
            break;
          }
        case ControllerSubFunc.db_select_batch:
          {
            await execSelectBatch(subFunc, query, params);
            break;
          }
        default:
          {
            return false;
          }
      }
      return true;
    } on Exception {
      return false;
    }
  }

  String _getMethod(params) {
    if (params.length == 3) {
      return params[2]["calledMethod"];
    } else {
      return "";
    }
  }

  Future<bool> execSelect(subFunc, query, params) async {
    String calledMethod = _getMethod(params);
    try {
      await checkDBStatus();
      clearResult();
      List<Map> list = await db.rawQuery(query, params[1]);
      result = list;
      _listener.sqlLiteResponse(subFunc, {
        'response_state': true,
        'calledMethod': calledMethod,
        'response_data': result
      });
      return true;
    } on Exception {
      _listener.sqlLiteResponse(subFunc, {
        'response_state': false,
        'calledMethod': calledMethod,
        'response_data': null
      });
      return false;
    }
  }

  Future<bool> execSelectBatch(subFunc, query, params) async {
    String calledMethod = _getMethod(params);
    List _res = [];
    try {
      await checkDBStatus();
      clearResult();

      for (int i = 0; i < query.length; i++) {
        List<Map> _list = await db.rawQuery(query[i], params[1][i]);
        _res.add(_list);
      }

      result = _res;
      _listener.sqlLiteResponse(subFunc, {
        'response_state': true,
        'calledMethod': calledMethod,
        'response_data': result
      });
      return true;
    } on Exception {
      _listener.sqlLiteResponse(subFunc, {
        'response_state': false,
        'calledMethod': calledMethod,
        'response_data': null
      });
      return false;
    }
  }

  Future<bool> execInsert(subFunc, query, params) async {
    String calledMethod = _getMethod(params);
    try {
      await checkDBStatus();
      await db.transaction((txn) async {
        await txn.rawInsert(query, params[1]);
      });
      _listener.sqlLiteResponse(subFunc, {
        'response_state': true,
        'calledMethod': calledMethod,
        'response_data': null
      });
      return true;
    } on Exception {
      _listener.sqlLiteResponse(subFunc, {
        'response_state': false,
        'calledMethod': calledMethod,
        'response_data': null
      });
      return false;
    }
  }

  Future<bool> execUpdate(subFunc, query, params) async {
    String calledMethod = _getMethod(params);
    try {
      await checkDBStatus();
      await db.transaction((txn) async {
        await txn.rawUpdate(query, params[1]);
      });
      _listener.sqlLiteResponse(subFunc, {
        'response_state': true,
        'calledMethod': calledMethod,
        'response_data': null
      });
      return true;
    } on Exception {
      _listener.sqlLiteResponse(subFunc, {
        'response_state': false,
        'calledMethod': calledMethod,
        'response_data': null
      });
      return false;
    }
  }

  Future<bool> execDelete(subFunc, query, params) async {
    String calledMethod = _getMethod(params);
    try {
      await checkDBStatus();
      await db.transaction((txn) async {
        await txn.rawDelete(query, params[1]);
      });
      _listener.sqlLiteResponse(subFunc, {
        'response_state': true,
        'calledMethod': calledMethod,
        'response_data': null
      });
      return true;
    } on Exception {
      _listener.sqlLiteResponse(subFunc, {
        'response_state': false,
        'calledMethod': calledMethod,
        'response_data': null
      });
      return false;
    }
  }

  checkDBStatus() async {
    if (db == null) {
      await init_db();
    }
  }
}

abstract class SQLiteControllerListener {
  sqlLiteResponse(subFunc, _response);
}
